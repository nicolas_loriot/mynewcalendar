import Icon from 'react-native-vector-icons/FontAwesome';
import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import App from './App'
import {UserComponent, CreateEventComponent, CreateCalendarComponent} from "./components";
import {HOME, CALENDAR, EVENT, USER} from "./constants/constants";

const NewApp = createBottomTabNavigator({
        Home: {screen: App},
        Event : {screen: CreateEventComponent},
        User: {screen: UserComponent},
        Calendar: {screen: CreateCalendarComponent}

    }, {
        navigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, tintColor}) => {
                const {routeName} = navigation.state;
                let iconName;
                if (routeName === HOME)
                    iconName = 'home';
                else if (routeName === EVENT)
                    iconName = 'calendar'
                else if (routeName === USER)
                    iconName = 'user'
                else if (routeName === CALENDAR)
                    iconName = 'plus'
                return <Icon name={iconName} size={25} color={tintColor}/>;
            },
        }),
        tabBarOptions: {
            activeBackgroundColor: 'orange',
            tintColor: 'black'
        },
    }
);

export default NewApp;