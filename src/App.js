import React, {Component} from 'react';
import {StyleSheet, Text, View, NetInfo} from 'react-native';
import axios from 'axios';
import {Card} from 'react-native-elements'

import {EventComponent, RightSideComponent, GoogleSignInComponent} from './components/index'

const effectiveType = {
    _2g: '2g',
    _3g: '3g',
    _4g: '4g'
}

const connectionType = {
    none: 'none',
    wifi: 'wifi',
    cellular: 'cellular',
    unknown: 'unknown'
}

interface ConnectionType {
    effectiveType: string,
    type: string
}
const client = axios.create({
    baseURL: 'https://newsapi.org/v2',
    responseType: 'json'
});



export default class App extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isConnected: false,
            netInfo: ''
        }
    }

    componentDidMount() {
        NetInfo.addEventListener('connectionChange', this._addEventListener);
/*
        NetInfo.isConnected.fetch().then(c => {
            this.setState({netInfo: c});
        }).then(NetInfo.getConnectionInfo().then(e => {
            this.setState({e})
        }))
        */
    }

    _addEventListener = (e: ConnectionType) => {
        var connected: boolean = false;
        if (e) {
            switch (e.type) {
                case connectionType.none:
                    connected = false;
                    break
                case connectionType.unknown:
                    connected = false;
                    break
                case connectionType.wifi:
                    connected = true;
                    break
                case connectionType.cellular:
                    switch (e.effectiveType) {
                        case effectiveType._2g:
                            connected = false;
                            break
                        case effectiveType._3g:
                            connected = true;
                            break
                        case effectiveType._4g:
                            connected = true;
                            break
                        default:
                            connected = false
                    }
                    break
                default:
                    connected = false;
                    break;
            }
        }
        this.setState({isConnected: connected})
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <GoogleSignInComponent/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    calendarLeft: {
        flex: 1,
        width: '55%'
    },
    layoutRight: {
        flex: 1,
        flexDirection: 'column'
    }
});

/*
                                <Card title='NEXT EVENT'>
                    <Text>
                        {JSON.stringify(this.state)}
                    </Text>
                </Card>
                <View style={styles.container}>
                    <EventComponent myStyle={styles.calendarLeft}/>
                    <RightSideComponent myStyle={styles.layoutRight}/>
                </View>
 */