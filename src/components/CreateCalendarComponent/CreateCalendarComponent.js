import React, {Component} from 'react';
import {PermissionsAndroid, View, Button} from 'react-native';
import Contacts from 'react-native-contacts';
import {IContacts} from "../../interfaces";
import AutoTags from 'react-native-tag-autocomplete';
import {EVENT} from "../../constants/constants";

class CreateCalendarComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contacts: [],
            selectedContacts: []
        }
    }

    async requestContactPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Cool Photo App Camera Permission',
                    'message': 'Cool Photo App needs access to your camera ' +
                    'so you can take awesome pictures.'
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return true
            } else {
                return false
            }
        } catch (err) {
            console.warn(err)
        }
    }

    getContacts() {
        var data = [];
        this.requestContactPermission().then(granted => {
            if (granted)
                data = Contacts.getAll((err, contacts) => {
                    if (err) return 'err';
                    contacts.map(contact => {

                    })
                    this.setState({contacts: this.transformContacts(contacts)})
                })
        })
        return data
    }

    transformContacts = (contacts: IContacts[]) => {
        let readableContacts = []
        contacts.forEach(contact => {
            if (this.checkCommunicationWay(contact.emailAddresses) || this.checkCommunicationWay(contact.phoneNumbers)) {
                readableContacts.push(this.mapContactPhone(contact))
            }
        })
        return readableContacts;
    }

    checkCommunicationWay = (communicationWays: any[]) => {
        if (!communicationWays.length)
            return false
        return true
    }

    componentDidMount() {
        this.getContacts();
    }

    mapContactPhone = (phoneContact: IContacts) => {
        let readableContact: IContacts = {};
        readableContact.phoneNumbers = phoneContact.phoneNumbers;
        readableContact.emailAddresses = phoneContact.emailAddresses;
        readableContact.familyName = phoneContact.familyName;
        readableContact.givenName = phoneContact.givenName;
        readableContact.name = phoneContact.givenName
        return readableContact;
    }

    handleAddition = suggestion => {
        this.setState({selectedContacts: this.state.selectedContacts.concat([suggestion])});
    }

    handleDelete = index => {
        let selectedContacts = this.state.selectedContacts;
        selectedContacts.splice(index, 1);
        this.setState({selectedContacts});
    }

    addCalendar = () => {
        this.props.navigation.navigate(EVENT)
    }

    goBack = () => {
        this.props.navigation.navigate(EVENT)
    }
    render() {
        const {contacts, selectedContacts} = this.state
        //const element = this.buildElement(contacts)
        return (
            <View>
                <View>
                    <AutoTags
                        suggestions={contacts}
                        tagsSelected={selectedContacts}
                        handleAddition={this.handleAddition}
                        handleDelete={this.handleDelete}
                        placeholder="Add a contact.."/>
                </View>
                <Button onPress={this.addCalendar} title="Ajouter"/>
                <Button onPress={this.goBack} title="Annuler"/>
            </View>
        );
    }
}

export default CreateCalendarComponent;

/*
contacts.map((contact, key) => {
                <Card title={contact.givenName} key={'contact'.concat(key.toString())}>
                    <Text>
                        {contact.phoneNumbers.length ? contact.phoneNumbers[0].number : contact.emailAddresses[0].email}
                    </Text>
                </Card>
 */