import React, {Component} from 'react';
import {View,} from 'react-native';
import {RightSideCard} from "../../stateless-components";
import PropTypes from 'prop-types';

class RightSideComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            breakingNews: '',
            horoscope: ''
        }
    }

    componentDidMount() {
        this.fetchHoroscope().done()
    }

    canRender(news, horoscope) {
        if (news && news.breakingNews && horoscope && horoscope.horoscope)
            return true
        return false;
    }

    async fetchHoroscope() {
        await this.setState({breakingNews: 'ma breakingNews', horoscope: 'mon horoscope'})
    }

    render() {
        const {myStyle} = this.props;
        const {breakingNews, horoscope} = this.state
        return (
            <View style={myStyle}>
                <RightSideCard renderText={horoscope} title="horoscope"/>
                <RightSideCard renderText={breakingNews} title="breaking news"/>
            </View>
        );
    }
}

RightSideComponent.propTypes = {
    myStyle: PropTypes.any,
};

export default RightSideComponent;
