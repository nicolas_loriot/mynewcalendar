import React, {Component} from 'react';
import {ScrollView, SectionList, StyleSheet, Text, View} from 'react-native';
import {Card} from 'react-native-elements'
import _ from 'lodash';
import PropTypes from 'prop-types';

class EventComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            events: []
        }
    }

    componentDidMount() {
        this.fetchEvents().done()
    }

    async fetchEvents() {
        await this.setState({events: fromArrayToSectionData(data)})
    }

    render() {
        const {myStyle} = this.props
        const {events} = this.state
        return (
            <ScrollView style={myStyle}>
                {events.length ?
                    <View>
                        <SectionList
                            sections={events}
                            renderItem={this._renderItem}
                            renderSectionHeader={this._renderSection}
                            keyExtractor={item => item.description}
                            ListEmptyComponent={this._renderEmpty}>
                        </SectionList>
                    </View> :
                    <Card>
                        <Text>Quelle vie calme...</Text>
                    </Card>
                }
            </ScrollView>
        );
    }

    _renderHeader = () => (
        <View
            style={{height: 30, backgroundColor: '#4fc3f7', justifyContent: 'center'}}
        >
            <Text>Header</Text>
        </View>
    )

    _renderFooter = () => (
        <View
            style={{height: 30, backgroundColor: '#4fc3f7', justifyContent: 'center'}}
        >
            <Text>Footer</Text>
        </View>
    )

    _renderEmpty = () => (
        <View style={{height: 40, alignItems: "center", justifyContent: "center"}}>
            <Card>Aucun résultat</Card>
        </View>
    );

    _renderSection = ({section}) => (
        <View style={styles.sectionContainer}>
            <View style={styles.leftLine}></View>
            <View style={styles.dateContainer}><Text style={styles.textStyle}>{section.key.toUpperCase()}</Text></View>
            <View style={styles.rightLine}></View>
        </View>
    )

    _renderItem = ({item}) => <Card
        title={new Date(item.time).toLocaleTimeString()}><Text>{item.description} + {JSON.stringify(new Date('2011-04-12T10:20:30Z').toLocaleDateString())}</Text></Card>

}

function fromArrayToSectionData(data) {

    let ds = _.groupBy(data, d => new Date(d.time).toLocaleDateString());
    ds = _.reduce(
        ds,
        (acc, next, index) => {
            acc.push({
                key: index,
                data: next
            });
            return acc;
        },
        []
    );
    ds = _.orderBy(ds, ["key"]);
    return ds;
}

EventComponent.propTypes = {
    myStyle: PropTypes.any,
};

const styles = StyleSheet.create({
    sectionContainer: {
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    leftLine: {
        height: 1,
        backgroundColor: 'black',
        width: '20%',
        marginTop: 20
    },
    rightLine: {
        height: 1,
        backgroundColor: 'black',
        width: '20%',
        marginTop: 20
    },
    dateContainer: {
        height: 40,
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',

    },
    textStyle: {
        fontSize: 20
    }
});

const data = [
    {time: '2011-04-12T10:20:30Z', title: '09:00', description: 'Event 1 Description'},
    {time: '2011-04-12T11:20:30Z', title: 'Event 2', description: 'Event 2 Description'},
    {time: '2011-04-12T12:20:30Z', title: 'Event 3', description: 'Event 3 Description'},
    {time: '2011-04-13T10:20:30Z', title: '09:00', description: 'Event 1 Description'},
    {time: '2011-04-13T11:20:30Z', title: 'Event 2', description: 'Event 2 Description'},
    {time: '2011-04-13T12:20:30Z', title: 'Event 3', description: 'Event 3 Description'},
    {time: '2011-04-14T10:20:30Z', title: '09:00', description: 'Event 1 Description'},
    {time: '2011-04-14T11:20:30Z', title: 'Event 2', description: 'Event 2 Description'},
    {time: '2011-04-14T12:20:30Z', title: 'Event 3', description: 'Event 3 Description'},
    {time: '2011-04-15T10:20:30Z', title: '09:00', description: 'Event 1 Description'},
    {time: '2011-04-15T11:20:30Z', title: 'Event 2', description: 'Event 2 Description'},
    {time: '2011-04-15T12:20:30Z', title: 'Event 3', description: 'Event 3 Description'},
    {time: '2011-04-10T10:20:30Z', title: '09:00', description: 'Event 1 Description'},
    {time: '2011-04-10T11:20:30Z', title: 'Event 2', description: 'Event 2 Description'},
    {time: '2011-04-10T12:20:30Z', title: 'Event 3', description: 'Event 3 Description'},
]

export default EventComponent;
/*
                {events.length ?
                    <View>
                        <SectionList
                            sections={events}
                            renderItem={this._renderItem}
                            renderSectionHeader={this._renderSection}
                            keyExtractor={item => item.description}
                            ListEmptyComponent={this._renderEmpty}>
                        </SectionList>
                    </View> :
                    <Card>
                        <Text>Quelle vie calme...</Text>
                    </Card>
                }
 */