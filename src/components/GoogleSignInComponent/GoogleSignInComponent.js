import React, {Component} from 'react';
import { GoogleSignin, statusCodes, GoogleSigninButton } from 'react-native-google-signin';
import {View, Text, ScrollView, Button, TextInput} from 'react-native'
import firebase from 'react-native-firebase';
import PropTypes from 'prop-types';

const email = 'loriot_n@etna-alternance.net'
const password = 'password'
class GoogleSignInComponent extends Component {

    onLoginOrRegister = () => {
        GoogleSignin.signIn()
            .then((data) => {
                // Create a new Firebase credential with the token
                const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
                // Login with the credential
                return firebase.auth().signInWithCredential(credential);
            })
            .then((user) => {
                // If you need to do anything with the user, do it here
                // The user will be logged in automatically by the
                // `onAuthStateChanged` listener we set up in App.js earlier
                this.setState({user})
            })
            .catch((error) => {
                const { code, message } = error;
                this.setState({code: message})
                // For details of error codes, see the docs
                // The message contains the default Firebase string
                // representation of the error
            });
    }

    onLogin = () => {
        //const { email, password } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => {
                this.setState({user})
            })
            .catch((error) => {
                const { code, message } = error;
                this.setState({code: code, error: message})
            });
    }
    onRegister = () => {
        //const { email, password } = this.state;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((user) => {
                this.setState({user})
            })
            .catch((error) => {
                const { code, message } = error;
                this.setState({code: code, error: message})
            });
    }

    componentDidMount() {
        GoogleSignin.configure({
            webClientId: '873841734723-pug9p2vikqmsth2mjkprqmuiqk955tqf.apps.googleusercontent.com'
        });
    }

    onChangeEmail = e => {
        this.setState({email: e})
    }

    onChangePassword = e => {
        this.setState({password: e})
    }
    render() {
        return (
            <ScrollView>
                <TextInput onChangeText={(e) => this.onChangeEmail(e)}/>
                <TextInput onChangeText={(e) => this.onChangePassword(e)}/>
                <Button title="register" onPress={this.onRegister}>

                </Button>
                <Button title="connect" onPress={this.onLogin}>

                </Button>

                <Text>
                    {JSON.stringify(this.state)}
                </Text>
            </ScrollView>
        );
    }
}

GoogleSignInComponent.propTypes = {};

export default GoogleSignInComponent;

/*
<GoogleSigninButton
                    style={{ width: 48, height: 48 }}
                    size={GoogleSigninButton.Size.Icon}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this.onLoginOrRegister}
                     />
 */