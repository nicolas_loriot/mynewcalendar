import React, {Component} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import t from 'tcomb-form-native';
import {CALENDAR, HOME} from "../../constants/constants";

var Calendars = t.enums({
    Principal: 'Principal',
    Second_calendrier: 'Second calendrier'
});

const Calendar = t.struct({
    myObjet: t.String,
    details: t.String,
    eventDate: t.Date,
    eventTime: t.Date,
    calendars: Calendars
});

const options = {
    auto: 'placeholders',
    fields: {
        eventDate: {
            config: {
                defaultValueText: <Text></Text>
            },

            label: 'Selectionnez une date',
            mode: 'date' // display the Date field as a DatePickerAndroid
        },
        eventTime: {
            config: {
                defaultValueText: <Text></Text>
            },

            label: 'Selectionnez une heure',
            mode: 'time'
        },
        myObjet: {
            label: 'Objet de l\'évènement'
        },
        details: {
            label: 'Détails de l\'évènement'
        },
        calendars: {
            nullOption: {value: '', text: 'Choisissez un calendrier'}
        }
    }
}


const Form = t.form.Form;

class CreateEventComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            event: null
        }
    }

    onChange = (value) => {
        this.setState({event: value}) // value here is an instance of Calendar
    }

    onPress = () => {
        var value = this.refs.form.getValue();
        if (value) { // if validation fails, value will be null
            this.saveUser(value).then(this.props.navigation.navigate(HOME), this.clearForm());
        }
    }

    clearForm = () => {
        this.setState({event: null})
    }
    goBack = () => {
        this.clearForm();
        this.props.navigation.navigate(HOME);
    }

    addCalendar = () => {
        this.props.navigation.navigate(CALENDAR)
    }

    async saveUser(user) {
        await console.log(user)
    }

    render() {
        return (
            <View style={styles.container}>
                <Button style={styles.button} onPress={this.addCalendar} title="Ajouter un calendrier"/>
                <Form type={Calendar} onChange={this.onChange} options={options} value={this.state.event} ref="form"/>
                <View style={styles.buttonContainer}>
                    <View style={[styles.buttonWidth, styles.buttonLeft]}>
                        <Button style={styles.buttonLeft} onPress={this.onPress} title="Sauvegarder"/>
                    </View>
                    <View style={[styles.buttonWidth, styles.buttonRight]}>
                        <Button style={styles.buttonRight} onPress={this.goBack} title="Annuler"/>
                    </View>
                </View>
            </View>
        );
    }
}

CreateEventComponent.propTypes = {};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        flex: 1,
        flexDirection: 'column'
    },
    buttonContainer: {
        flex: 1,
        flexDirection: 'row',
    },
    buttonWidth: {
        width: '50%',
    },
    buttonLeft: {
        paddingRight: 2
    },
    buttonRight: {
        paddingLeft: 2
    }
});

export default CreateEventComponent;