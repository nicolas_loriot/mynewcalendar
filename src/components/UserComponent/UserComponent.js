import React, {Component} from 'react';
import {StyleSheet, View,} from 'react-native';
import t from 'tcomb-form-native';

const User = t.struct({
    email: t.String,
    username: t.String,
    password: t.String,
    terms: t.Boolean
});

const Form = t.form.Form;

class UserComponent extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Form type={User}/>
            </View>
        );
    }
}

UserComponent.propTypes = {};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    },
});

export default UserComponent;
