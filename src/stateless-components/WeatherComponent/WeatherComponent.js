import React from 'react';
import {Text} from 'react-native';
import {Card} from 'react-native-elements'
import {IAccuWeather} from '../../interfaces/index'

const getWeatherIcon = (weatherString) => {
    let icon = null;
    switch (weatherString) {
        case "beau":
            icon = "il fait beau"
            break
        case "variable":
            icon = "des nuages"
            break
        default:
            icon = "moche"
            break
    }
    return icon
}

const WeatherComponent = ({accuWeather}) => {
    const weather: IAccuWeather = accuWeather ? accuWeather : null;
    const icon = getWeatherIcon(weather)
    return (
        <Card>
            <Text>{icon}</Text>
        </Card>
    );
};

export default WeatherComponent;
