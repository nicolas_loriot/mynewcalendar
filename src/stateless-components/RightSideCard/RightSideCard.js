import React from 'react';
import {Text,} from 'react-native';
import {Card} from 'react-native-elements'
import PropTypes from 'prop-types';

export const RightSideCard = ({renderText, title}) => {
    return (
        <Card title={title}>
            <Text>
                {renderText}
            </Text>
        </Card>
    );
}

RightSideCard.propTypes = {
    title: PropTypes.string.isRequired,
    renderText: PropTypes.string.isRequired
};

export default RightSideCard;
