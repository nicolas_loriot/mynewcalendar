export interface IContacts {
    phoneNumbers: IPhoneNumber[];
    emailAddresses: IEmailAdresse[];
    givenName: string;
    familyName: string;

}

export interface IPhoneNumber {
    id: string;
    label: string;
    number: string;
}

export interface IEmailAdresse {
    id: string;
    label: string;
    email: string;
}