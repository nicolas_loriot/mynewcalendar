/* props interfaces */

export {ICreateElementComponentProps} from './interfaces/PropsInterface/ICreateElementComponentProps'

/* state interface */

export {ICreateElementComponentState} from './interfaces/StateInterface/ICreateElementComponentState'

/* other interface */

export { IAccuWeather } from './interfaces/OtherInterface/IAccuWeather';
export { IContacts } from  './interfaces/OtherInterface/IContacts'