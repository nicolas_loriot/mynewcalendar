/* api keys */

export const ACCU_WEATHER_KEY = "bp5PlipEul7VTP4HwqNTAlsy17Gr8qJr"

/* api routes */

export const BASE_WEATHER_ROUTE = "http://dataservice.accuweather.com/"
export const AUTO_COMPLETE_GET_TOWN = BASE_WEATHER_ROUTE + "locations/v1/cities/autocomplete?apikey=" + ACCU_WEATHER_KEY + "&q=";

/* app route */

export const HOME = 'Home';
export const CALENDAR = 'Calendar';
export const USER = 'User';
export const EVENT = 'Event'